<%@ page language="java" contentType="text/html; charset=ISO-8859-1" isELIgnored="false"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<header class="page-header">
<h4>Liste des emprunts<h4>
</header>
<p>Emprunt du membre : ${firstName} ${lastName}</p>
<p>
	<% 
		if (request.getParameter("borrow")!=null){
			String parametre = request.getParameter("borrow");
			out.println(parametre);
				}
	%>
</p>

<table class="table table-bordered table-striped table-condensed">
	<thead>
		<tr>
			<th><spring:message code="colonne.book" /></th>
			<th><spring:message code="colonne.status" /></th>
			<th><spring:message code="colonne.borrowDate" /></th>
			<th><spring:message code="colonne.limitDate" /></th>
			<th><spring:message code="colonne.returnedDate" /></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${borrowList}" var="borrow">
			<tr>
				<td><c:out value="${borrow.book.reference}" /></td>
				<td><c:choose>
				<c:when  test="${borrow.status == 1 }">
				<c:out value="En cours" />
				</c:when>
				<c:when  test="${borrow.status == 2 }">
				<c:out value="Rendu"/>
				</c:when>
				<c:otherwise>
				<c:out value="Prolong�"/>
				</c:otherwise>
				</c:choose></td>
				<td><c:out value="${borrow.borrowDate}" /></td>
				<td><c:if test="${borrow.status != 2}"><c:out value="${borrow.limitDate}" /></c:if></td>
				<td><c:if test="${borrow.status == 2}"><c:out value="${borrow.limitDate}" /></c:if></td>
				
				<td><c:if test="${borrow.status ==1}"> <a class="btn btn-success" href="extend/${borrow.borrowId}"> Prolonger</a></c:if></td>
			
<%-- 				<td> <c:if test="${borrow.status != 2 }"><a class="btn btn-warning" href="return/${borrow.borrowId}"> Rendre</a></c:if></td> --%>
			</tr>
		</c:forEach>
	</tbody>
</table>

