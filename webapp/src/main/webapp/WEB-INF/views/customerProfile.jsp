<%@ page language="java" contentType="text/html; charset=ISO-8859-1" isELIgnored="false"
         pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<header class="page-header">
    <h4>Profil utilisateur<h4>
</header>

<label>Nom
    <input disabled value="${customer.lastName}"/>
</label><br>
<label>Pr�nom
    <input disabled value="${customer.firstName}"/>
</label><br>
<label>Mail
    <input disabled value="${customer.mail}"/>
</label><br>
<label> Alerte avant expiration des pr�ts
    <c:if test="${customer.alert == true}" > <a class="btn btn-danger" href="alert/${customer.customerId}"> D�sactiver</a></c:if>
    <c:if test="${customer.alert == false}" > <a class="btn btn-success" href="alert/${customer.customerId}"> Activer</a></c:if>

</label>




