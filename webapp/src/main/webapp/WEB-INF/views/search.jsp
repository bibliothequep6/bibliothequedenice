<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<header class="page-header">
<h4>
	Recherche multi-crit�res
	<h4>
</header>


<form:form method="post" action="searchWork">
	<div class="form-row">
		<div class="form-group col-lg-4">
			<input type="text" value="" class="form-control"
				placeholder="saisissez un auteur" name="author">
		</div>
		<div class="form-group col-lg-4">
			<input type="text" class="form-control" value=""
				placeholder="saisissez un titre" name="title">
		</div>
		<div class="form-group col-lg-4">
			<input type="number" class="form-control" value=""
				placeholder="saisissez une ann�e" name="year">
		</div>
		<input type="submit" class="btn btn-info" />
	</div>
</form:form>

