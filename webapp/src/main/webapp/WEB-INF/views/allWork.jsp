<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         isELIgnored="false" pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<header class="page-header">
    <h4>
        Liste des oeuvres
    </h4>
</header>
<table class="table table-bordered table-striped table-condensed">
    <thead>
    <tr>
        <th><spring:message code="colonne.title"/></th>
        <th><spring:message code="colonne.author"/></th>
        <th><spring:message code="colonne.summary"/></th>
        <th><spring:message code="colonne.yearpublication"/></th>
        <th><spring:message code="colonne.dispo"/></th>
        <th><spring:message code="colonne.reservation"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${allWork}" var="work">
        <tr>
            <td><c:out value="${work.title}"/></td>
            <td><c:out value="${work.author}"/></td>
            <td><c:out value="${work.summary}"/></td>
            <td><c:out value="${work.yearPublication}"/></td>
            <td>
                <c:set var="count" value="0"/><c:set var="returnDate" value="null"/>
                <c:forEach items="${work.bookList}" var="book">
                    <c:if test="${book.available == true }">
                        <c:set var="count" value="${count + 1 }"/>
                    </c:if>
                </c:forEach> <c:choose>
                <c:when test="${count == 0 }">
                    <c:out value="Indisponible, retour pr�vu le : ${work.nextReturnDate}"/>
                </c:when>
                <c:otherwise>
                    <c:out value="${count}"/><c:out value=" En stock"/>
                </c:otherwise>
            </c:choose>
            </td>
            <td>
                <c:if test="${count == 0}">
                    <a class="btn btn-info" href="booking/${work.workId}">Reserver l'ouvrage - Capacit� : ${work.bookingList.size()}/${(work.stockQuantity)*2 }</a>
                </c:if>
            </td>
            <c:forEach items="${work.bookingList}" var="booking">
                <c:if test="${customer.customerId == booking.customer.customerId}">
                    <td><a class="btn btn-danger" href="cancelBooking/${work.workId}/${booking.bookingId}">Annuler sa r�servation</a></td>
                </c:if>
            </c:forEach>

            <!-- 				<td> -->
                <%-- 			 <a class="btn btn-info" href="borrow/${work.workId}"> Emprunter</a></td> --%>
        </tr>
    </c:forEach>
    </tbody>
</table>

<p>
    <%
        if (request.getParameter("borrow") != null) {
            String parametre = request.getParameter("borrow");
            out.println(parametre);
        }
    %>
</p>