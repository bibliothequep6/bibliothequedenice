<%@ page language="java" contentType="text/html; charset=ISO-8859-1" isELIgnored="false"
         pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<header class="page-header">
    <h4>Liste des reservations<h4>
</header>
<p>Reservations du membre : ${customer.firstName} ${customer.lastName}</p>


<table class="table table-bordered table-striped table-condensed">
    <thead>
    <tr>
        <th><spring:message code="colonne.book" /></th>
        <th><spring:message code ="colonne.dispo"/></th>
        <th><spring:message code="colonne.position" /></th>
    </tr>
    </thead>
    <tbody>
        <c:forEach items="${bookingList}" var="booking">
            <tr>
                <td><c:out value="${booking.work.title}"/></td>
                <td><c:out value="${booking.work.nextReturnDate}"/></td>
                <td>${position}/${bookingList.size()}</td>
            </tr>
        </c:forEach>

    </tbody>
</table>

