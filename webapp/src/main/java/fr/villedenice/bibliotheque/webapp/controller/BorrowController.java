package fr.villedenice.bibliotheque.webapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fr.villedenice.bibliotheque.client.ws.BorrowInfo;
import fr.villedenice.bibliotheque.client.ws.CustomerInfo;
import fr.villedenice.bibliotheque.client.ws.EndBorrowRequest;
import fr.villedenice.bibliotheque.client.ws.EndBorrowResponse;
import fr.villedenice.bibliotheque.client.ws.ExtendBorrowRequest;
import fr.villedenice.bibliotheque.client.ws.ExtendBorrowResponse;
import fr.villedenice.bibliotheque.client.ws.GetAllBorrowRequest;
import fr.villedenice.bibliotheque.client.ws.GetAllBorrowResponse;
import fr.villedenice.bibliotheque.client.ws.ServiceStatus;
import fr.villedenice.bibliotheque.client.ws.StartBorrowRequest;
import fr.villedenice.bibliotheque.client.ws.StartBorrowResponse;
import fr.villedenice.bibliotheque.client.ws.WorkPort;
import fr.villedenice.bibliotheque.client.ws.WorkPortService;

@Controller
public class BorrowController {
	WorkPortService workPortService = new WorkPortService();
	WorkPort workPort = workPortService.getWorkPortSoap11();
	
	
    @RequestMapping(value="/borrow/{workId}", method = RequestMethod.GET)
    public ModelAndView borrow(HttpServletRequest request, @PathVariable Integer workId) {
    	ModelAndView modelBorrow = new ModelAndView();

    	CustomerInfo customerInfo = (CustomerInfo) request.getSession().getAttribute("user");
    	int customerId = customerInfo.getCustomerId();
    	StartBorrowRequest borrowRequest = new StartBorrowRequest();
    	borrowRequest.setWorkId(workId);
    	borrowRequest.setCustomerId(customerId);
    	
    	StartBorrowResponse borrowResponse = workPort.startBorrow(borrowRequest);
    	ServiceStatus status = borrowResponse.getServiceStatus();
    	modelBorrow.addObject("borrow", status.getMessage());
    	
    	modelBorrow.setViewName("redirect:/allwork");
//    	GetWorkByIdRequest getWorkByIdRequest = new GetWorkByIdRequest();
//    	getWorkByIdRequest.setWorkId(workId);
//    	GetWorkByIdResponse response = workPort.getWorkById(getWorkByIdRequest);
//    	List<WorkInfo> workInfo = response.getWorkInfo();
//    	modelBorrow.addObject("allWork", workInfo);
    	
    	return modelBorrow;
    	
    }
    @RequestMapping(value="/borrowList", method = RequestMethod.GET)
    public ModelAndView borrowList(HttpServletRequest request) {
    	ModelAndView modelBorrowList = new ModelAndView("borrowList");
    	GetAllBorrowRequest allBorrowRequest = new GetAllBorrowRequest();
    	CustomerInfo customerInfo = (CustomerInfo) request.getSession().getAttribute("user");
    	int customerId = customerInfo.getCustomerId();
    	allBorrowRequest.setCustomerId(customerId);
    	GetAllBorrowResponse allBorrowResponse = workPort.getAllBorrow(allBorrowRequest);
    	List<BorrowInfo> borrowList = allBorrowResponse.getBorrowInfo();
    	
    	modelBorrowList.addObject("firstName", customerInfo.getFirstName());
    	modelBorrowList.addObject("lastName", customerInfo.getLastName());
    	modelBorrowList.addObject("borrowList", borrowList);
    	return modelBorrowList ;  


}
    @RequestMapping(value="/extend/{borrowId}", method = RequestMethod.GET)
    public ModelAndView extend(HttpServletRequest request, @PathVariable Integer borrowId) {
    	ModelAndView modelBorrow = new ModelAndView();

    	
    	ExtendBorrowRequest extendBorrowRequest = new ExtendBorrowRequest();
    	extendBorrowRequest.setBorrowId(borrowId);
    	
    	ExtendBorrowResponse extendBorrowResponse = workPort.extendBorrow(extendBorrowRequest);
    	ServiceStatus status = extendBorrowResponse.getServiceStatus();
    	modelBorrow.addObject("borrow", status.getMessage());
    	
    	modelBorrow.setViewName("redirect:/borrowList");

    	
    	return modelBorrow;
    	
    }
    @RequestMapping(value="/return/{borrowId}", method = RequestMethod.GET)
    public ModelAndView endBorrow(HttpServletRequest request, @PathVariable Integer borrowId) {
    	ModelAndView modelBorrow = new ModelAndView();

    	EndBorrowRequest endBorrowRequest = new EndBorrowRequest();
    	endBorrowRequest.setBorrowId(borrowId);
    	
    	EndBorrowResponse endBorrowResponse = workPort.endBorrow(endBorrowRequest);
    	ServiceStatus status = endBorrowResponse.getServiceStatus();
    	modelBorrow.addObject("borrow", status.getMessage());
    	
    	modelBorrow.setViewName("redirect:/borrowList");
    	
    	
    	return modelBorrow;
    	
    }
}
