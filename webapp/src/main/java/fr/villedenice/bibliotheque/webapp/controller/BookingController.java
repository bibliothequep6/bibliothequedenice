package fr.villedenice.bibliotheque.webapp.controller;

import fr.villedenice.bibliotheque.client.ws.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class BookingController {

    WorkPortService workPortService = new WorkPortService();
    WorkPort workPort = workPortService.getWorkPortSoap11();
    @RequestMapping(value="/bookingList", method=RequestMethod.GET)
    public ModelAndView bookingList(HttpServletRequest request){
        ModelAndView modelBookingList = new ModelAndView();
        CustomerInfo customerInfo = (CustomerInfo) request.getSession().getAttribute("user");
        System.out.println(customerInfo.getFirstName());
        GetAllBookingRequest bookingRequest = new GetAllBookingRequest();
        bookingRequest.setCustomerId(customerInfo.getCustomerId());
        GetAllBookingResponse response = workPort.getAllBooking(bookingRequest);
        List<BookingInfo> bookingInfoList = response.getBookingInfo();
        //todo passer dans le service
        for (int i=0; i<bookingInfoList.size(); i++){
            if (bookingInfoList.get(i).getCustomer().getCustomerId()== customerInfo.getCustomerId()){
                modelBookingList.addObject("position", i+1);
            }
        }
        modelBookingList.addObject("customer",customerInfo);
        modelBookingList.addObject("bookingList", bookingInfoList);
        return modelBookingList;
    }

    @RequestMapping(value="/booking/{workId}", method = RequestMethod.GET)
    public ModelAndView booking(HttpServletRequest request, @PathVariable Integer workId){
        ModelAndView modelBookingList = new ModelAndView();

        CustomerInfo customerInfo = (CustomerInfo) request.getSession().getAttribute("user");
        int customerId = customerInfo.getCustomerId();

        BookingRequest bookingRequest = new BookingRequest();
        bookingRequest.setCustomerId(customerId);
        bookingRequest.setWorkId(workId);
        BookingResponse bookingResponse= workPort.booking(bookingRequest);
        ServiceStatus status = bookingResponse.getServiceStatus();
        modelBookingList.addObject("bookingStatus", status.getMessage());
        modelBookingList.addObject("customer", customerInfo);
        modelBookingList.setViewName("redirect:/allwork");
        return modelBookingList;
    }
    /*@RequestMapping(value="/cancelBooking/{workId}/{bookingId}", method = RequestMethod.DELETE)
	public ModelAndView cancelBooking(HttpServletRequest request, @PathVariable Integer workId, @PathVariable Integer bookingId){
    	ModelAndView modelBooking = new ModelAndView();


	}*/
}
