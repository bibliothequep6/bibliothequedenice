package fr.villedenice.bibliotheque.webapp.controller;

import fr.villedenice.bibliotheque.client.ws.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomerController {

    WorkPortService workPortService = new WorkPortService();
    WorkPort workPort = workPortService.getWorkPortSoap11();

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public ModelAndView afficher(HttpServletRequest request) {
        ModelAndView profileModel = new ModelAndView("customerProfile");
        CustomerInfo customerInfo = (CustomerInfo) request.getSession().getAttribute("user");
        profileModel.addObject("customer", customerInfo);
        return profileModel;
    }

    @RequestMapping(value = "/alert/{customerId}", method = RequestMethod.GET)
    public ModelAndView alert(HttpServletRequest request, @PathVariable Integer customerId) {
        ModelAndView profileModel = new ModelAndView();
        SetAlertRequest alertRequest = new SetAlertRequest();
        alertRequest.setCustomerId(customerId);
        SetAlertResponse alertResponse = workPort.setAlert(alertRequest);
        System.out.println(alertResponse.getCustomerInfo().isAlert());
        ((CustomerInfo) request.getSession().getAttribute("user")).setAlert(alertResponse.getCustomerInfo().isAlert());
        profileModel.setViewName("redirect:/profile");

        return profileModel;
    }
}
