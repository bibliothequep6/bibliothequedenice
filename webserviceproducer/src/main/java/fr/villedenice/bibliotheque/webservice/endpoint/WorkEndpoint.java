package fr.villedenice.bibliotheque.webservice.endpoint;

import java.util.ArrayList;
import java.util.List;

import fr.villedenice.bibliotheque.model.*;
import fr.villedenice.bibliotheque.ws.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import fr.villedenice.bibliotheque.business.BorrowService;
import fr.villedenice.bibliotheque.business.CustomerService;
import fr.villedenice.bibliotheque.business.WorkService;

@Endpoint
public class WorkEndpoint {
    private static final String NAMESPACE_URI = "http://www.villedenicebibliotheque.fr/work-ws";
    @Autowired
    private WorkService workService;
    @Autowired
    private BorrowService borrowService;
    @Autowired
    private CustomerService customerService;

    //WORK SERVICE
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getWorkByFilterRequest")
    @ResponsePayload
    public GetWorkByFilterResponse getWorkByFilter(@RequestPayload GetWorkByFilterRequest request) {
        String author = request.getWorkInfo().getAuthor();
        String title = request.getWorkInfo().getTitle();
        Integer year = request.getWorkInfo().getYearPublication();

        GetWorkByFilterResponse response = new GetWorkByFilterResponse();
        List<WorkInfo> workInfoList = new ArrayList<>();

        List<Work> workList = workService.findByFilter(author, title, year);
        for (int i = 0; i < workList.size(); i++) {
            WorkInfo ob = new WorkInfo();
            List<Book> bookList = workList.get(i).getBooks();
            for (Book book : bookList) {
                BookInfo bookInfo = new BookInfo();
                BeanUtils.copyProperties(book, bookInfo);
                ob.getBookList().add(bookInfo);
            }
            BeanUtils.copyProperties(workList.get(i), ob);
            workInfoList.add(ob);
        }
        response.getWorkInfo().addAll(workInfoList);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getWorkByAuthorRequest")
    @ResponsePayload
    public GetWorkByAuthorResponse getWorkByAuthor(@RequestPayload GetWorkByAuthorRequest request) {
        GetWorkByAuthorResponse response = new GetWorkByAuthorResponse();
        List<WorkInfo> workInfoListByAuthor = new ArrayList<>();
        List<Work> workList = workService.findByAuthor(request.getAuthor());
        for (int i = 0; i < workList.size(); i++) {
            WorkInfo ob = new WorkInfo();
            BeanUtils.copyProperties(workList.get(i), ob);
            workInfoListByAuthor.add(ob);
        }
        response.getWorkInfo().addAll(workInfoListByAuthor);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getWorkByIdRequest")
    @ResponsePayload
    public GetWorkByIdResponse getWorkById(@RequestPayload GetWorkByIdRequest request) {
        GetWorkByIdResponse response = new GetWorkByIdResponse();
        WorkInfo workInfo = new WorkInfo();
        Work work = workService.findByWorkId(request.getWorkId());
        BeanUtils.copyProperties(work, workInfo);
        response.getWorkInfo().add(workInfo);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getWorkByTitleRequest")
    @ResponsePayload
    public GetWorkByTitleResponse getWorkByTitle(@RequestPayload GetWorkByTitleRequest request) {
        GetWorkByTitleResponse response = new GetWorkByTitleResponse();
        List<WorkInfo> workInfoListByTitle = new ArrayList<>();
        List<Work> workList = workService.findByTitle(request.getTitle());
        for (int i = 0; i < workList.size(); i++) {
            WorkInfo ob = new WorkInfo();
            BeanUtils.copyProperties(workList.get(i), ob);
            workInfoListByTitle.add(ob);
        }
        response.getWorkInfo().addAll(workInfoListByTitle);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getWorkByDateRequest")
    @ResponsePayload
    public GetWorkByDateResponse getWorkByDate(@RequestPayload GetWorkByDateRequest request) {
        GetWorkByDateResponse response = new GetWorkByDateResponse();
        List<WorkInfo> workInfoListByDate = new ArrayList<>();
        List<Work> workList = workService.findByYearPublication(request.getDate());
        for (int i = 0; i < workList.size(); i++) {
            WorkInfo ob = new WorkInfo();
            BeanUtils.copyProperties(workList.get(i), ob);
            workInfoListByDate.add(ob);
        }
        response.getWorkInfo().addAll(workInfoListByDate);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllWorkRequest")
    @ResponsePayload
    public GetAllWorkResponse getAllWork() {
        GetAllWorkResponse response = new GetAllWorkResponse();
        List<WorkInfo> workInfoList = new ArrayList<>();
        List<Work> workList = workService.getAllWork();
        for (int i = 0; i < workList.size(); i++) {
            WorkInfo ob = new WorkInfo();
            String nextReturnDate = workService.getNextReturnDate(workList.get(i));
            List<Book> bookList = workList.get(i).getBooks();
            List<Booking> bookingList = workList.get(i).getBookingList();

            //todo refacto
            for (Book book : bookList) {
                BookInfo bookInfo = new BookInfo();
                BeanUtils.copyProperties(book, bookInfo);
                ob.getBookList().add(bookInfo);
            }
            //todo refacto
            for (Booking booking : bookingList) {
                BookingInfo bookingInfo = new BookingInfo();
                BeanUtils.copyProperties(booking, bookingInfo);
                CustomerInfo customerInfo = new CustomerInfo();
                BeanUtils.copyProperties(booking.getCustomer(), customerInfo);
                bookingInfo.setCustomer(customerInfo);
                ob.getBookingList().add(bookingInfo);
            }
            BeanUtils.copyProperties(workList.get(i), ob);
            if (nextReturnDate != null) {
                ob.setNextReturnDate(nextReturnDate);
            }

            workInfoList.add(ob);
        }
        response.getWorkInfo().addAll(workInfoList);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addWorkRequest")
    @ResponsePayload
    public AddWorkResponse addWork(@RequestPayload AddWorkRequest request) {
        AddWorkResponse response = new AddWorkResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        Work work = new Work();
        work.setTitle(request.getTitle());
        work.setAuthor(request.getAuthor());
        work.setSummary(request.getSummary());
        work.setYearPublication(request.getYearPublication());
        work.setStockQuantity(request.getStockQuantity());

        boolean flag = workService.addWork(work);
        if (flag == false) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Content Already Available");
            response.setServiceStatus(serviceStatus);
        } else {
            WorkInfo workInfo = new WorkInfo();
            BeanUtils.copyProperties(work, workInfo);

            response.setWorkInfo(workInfo);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
            response.setServiceStatus(serviceStatus);
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateWorkRequest")
    @ResponsePayload
    public UpdateWorkResponse updateWork(@RequestPayload UpdateWorkRequest request) {
        Work work = new Work();
        BeanUtils.copyProperties(request.getWorkInfo(), work);
        workService.updateWork(work);
        ServiceStatus serviceStatus = new ServiceStatus();
        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Content Updated Successfully");
        UpdateWorkResponse response = new UpdateWorkResponse();
        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "bookingRequest")
    @ResponsePayload
    public BookingResponse bookingWork(@RequestPayload BookingRequest request) {

        BookingResponse response = new BookingResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        boolean flag = workService.bookingWork(request.getWorkId(), request.getCustomerId());
        if (flag == false) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Booking is not possible for this work");
            response.setServiceStatus(serviceStatus);
        } else {
            BookingInfo bookingInfo = new BookingInfo();
            response.setBookingInfo(bookingInfo);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Booked Successfully");
            response.setServiceStatus(serviceStatus);
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "cancelBookingRequest")
    @ResponsePayload
    public CancelBookingResponse cancelBooking(@RequestPayload CancelBookingRequest request) {

        workService.cancelBooking(request.getBookingId(), request.getWorkId());
        CancelBookingResponse response = new CancelBookingResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Booking canceled succesfully");
        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllBookingRequest")
    @ResponsePayload
    public GetAllBookingResponse getAllBooking(@RequestPayload GetAllBookingRequest request) {
        GetAllBookingResponse bookingResponse = new GetAllBookingResponse();
        List<Booking> bookingList = customerService.getBookingList(request.getCustomerId());
        List<BookingInfo> bookingInfoList = new ArrayList<>();
        for(Booking booking : bookingList){
            Work work = booking.getWork();
            WorkInfo workInfo = new WorkInfo();
            workInfo.setNextReturnDate(workService.getNextReturnDate(work));
            BeanUtils.copyProperties(work, workInfo);
            CustomerInfo customerInfo = new CustomerInfo();
            BeanUtils.copyProperties(booking.getCustomer(), customerInfo);
            BookingInfo bookingInfo = new BookingInfo();
            bookingInfo.setBookingId(booking.getId());
            bookingInfo.setWork(workInfo);
            bookingInfo.setCustomer(customerInfo);
            bookingInfoList.add(bookingInfo);
        }
        bookingResponse.getBookingInfo().addAll(bookingInfoList);
        return bookingResponse;
    }

    //BORROW SERVICE
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBorrowByIdRequest")
    @ResponsePayload
    public GetBorrowByIdResponse getBorrow(@RequestPayload GetBorrowByIdRequest request) {
        GetBorrowByIdResponse response = new GetBorrowByIdResponse();
        BorrowInfo borrowInfo = new BorrowInfo();
        BeanUtils.copyProperties(borrowService.getBorrowById(request.getBorrowId()), borrowInfo);
        response.setBorrowInfo(borrowInfo);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "startBorrowRequest")
    @ResponsePayload
    public StartBorrowResponse startBorrow(@RequestPayload StartBorrowRequest request) {
        StartBorrowResponse response = new StartBorrowResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        boolean flag = borrowService.startBorrow(request.getWorkId(), request.getCustomerId());
        if (flag == false) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("No more books available for this work");
            response.setServiceStatus(serviceStatus);
        } else {
            BorrowInfo borrowInfo = new BorrowInfo();
            response.setBorrowInfo(borrowInfo);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Borrowed Successfully");
            response.setServiceStatus(serviceStatus);
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "extendBorrowRequest")
    @ResponsePayload
    public ExtendBorrowResponse extendBorrow(@RequestPayload ExtendBorrowRequest request) {
        Borrow borrow = borrowService.getBorrowById(request.getBorrowId());
        borrowService.extendBorrow(borrow);
        ServiceStatus serviceStatus = new ServiceStatus();
        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Borrow Extended Successfully");
        ExtendBorrowResponse response = new ExtendBorrowResponse();
        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "endBorrowRequest")
    @ResponsePayload
    public EndBorrowResponse endBorrow(@RequestPayload EndBorrowRequest request) {
        Borrow borrow = borrowService.getBorrowById(request.getBorrowId());
        borrowService.endBorrow(borrow);
        ServiceStatus serviceStatus = new ServiceStatus();
        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Borrow returned succesfully");
        EndBorrowResponse response = new EndBorrowResponse();
        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllBorrowRequest")
    @ResponsePayload
    public GetAllBorrowResponse getAllBorrow(@RequestPayload GetAllBorrowRequest request) {
        GetAllBorrowResponse response = new GetAllBorrowResponse();
        List<BorrowInfo> borrowInfoList = new ArrayList<>();

        List<Borrow> borrowList = customerService.getCustomerBorrowHistory(request.getCustomerId());
        for (int i = 0; i < borrowList.size(); i++) {
            BorrowInfo ob = new BorrowInfo();
            BookInfo bookInfo = new BookInfo();
            BeanUtils.copyProperties(borrowList.get(i).getBook(), bookInfo);
            BeanUtils.copyProperties(borrowList.get(i), ob);
            ob.setBorrowDate(borrowList.get(i).getBorrowDate().toString());
            ob.setLimitDate(borrowList.get(i).getLimitDate().toString());

            ob.setBook(bookInfo);

            borrowInfoList.add(ob);
        }

        response.getBorrowInfo().addAll(borrowInfoList);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "sendMailRequest")
    @ResponsePayload
    public SendMailResponse sendMail() {
        SendMailResponse sendMailResponse = new SendMailResponse();
        borrowService.sendMail(true, null);
        sendMailResponse.getServiceStatus().setMessage("SUCCESS");
        return sendMailResponse;
    }

    //CUSTOMERSERVICE
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "logInRequest")
    @ResponsePayload
    public LogInResponse logIn(@RequestPayload LogInRequest request) {
        LogInResponse response = new LogInResponse();
        CustomerInfo customerInfo = new CustomerInfo();
        boolean flag = customerService.logIn(request.getMail(), request.getPassword());
        if (flag == true) {
            Customer customer = customerService.findByMail(request.getMail());

            BeanUtils.copyProperties(customer, customerInfo);
            customerInfo.setConnected(true);
            response.setCustomerInfo(customerInfo);

        } else {
            customerInfo.setConnected(false);
            response.setCustomerInfo(customerInfo);
        }
        return response;
    }
    @PayloadRoot(namespace = NAMESPACE_URI, localPart="setAlertRequest")
    @ResponsePayload
    public SetAlertResponse response(@RequestPayload SetAlertRequest request){
        SetAlertResponse alertResponse = new SetAlertResponse();
        Customer customer = customerService.setAlert(request.getCustomerId());
        CustomerInfo customerInfo = new CustomerInfo();
        BeanUtils.copyProperties(customer, customerInfo);
        ServiceStatus status = new ServiceStatus();
        status.setMessage("Changement effectué");
        alertResponse.setServiceStatus(status);
        alertResponse.setCustomerInfo(customerInfo);
        return alertResponse;
    }
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCustomerByMailRequest")
    @ResponsePayload
    public GetCustomerByMailResponse getCustomerByMail(@RequestPayload GetCustomerByMailRequest request) {
        GetCustomerByMailResponse response = new GetCustomerByMailResponse();
        CustomerInfo customerInfo = new CustomerInfo();
        Customer customer = customerService.findByMail(request.getMail());
        BeanUtils.copyProperties(customer, customerInfo);
        response.setCustomerInfo(customerInfo);
        return response;
    }
}
