package fr.villedenice.bibliotheque.business;

import java.util.List;


import fr.villedenice.bibliotheque.model.Booking;
import fr.villedenice.bibliotheque.model.Work;

public interface WorkService {

	public List<Work> findByAuthor(String authorName);

	public List<Work> findByTitle(String workTitle);

	public List<Work> findByYearPublication(int yearPublication);

	public List<Work> getAllWork();

	public String getNextReturnDate(Work work);

	public boolean bookingWork(Integer workId, Integer customerId);

	public void cancelBooking(Integer bookingId, Integer workId);
	
	boolean addWork(Work work);
	
    void updateWork(Work work);
    
    public Work findByWorkId (int workId);
    
    public List<Work> findByFilter(String author, String title, Integer year);


 
}
