package fr.villedenice.bibliotheque.business;

import fr.villedenice.bibliotheque.model.Book;

public interface BookService {
	
	public Book getByBookId(Integer bookId);

	

}
