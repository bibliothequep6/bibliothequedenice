package fr.villedenice.bibliotheque.business;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import fr.villedenice.bibliotheque.model.*;

public interface BorrowService {
	public Borrow getBorrowById(Integer borrowId);
	/**
	 * emprunter l'exemplaire un livre
	 * 
	 * @param book -- exemplaire d'un livre
	 * @return -- retourne un emprunt
	 */
	public boolean startBorrow(Integer workId, Integer customerId);

	/**
	 * prolonge un emprunt
	 * 
	 * @param borrow -- emprunt
	 * @return -- emprunt prolong�
	 */
	public void extendBorrow(Borrow borrow);

	/**
	 * termine un emprunt (restitution exemplaire)
	 * 
	 * @param borrow -- emprunt
	 * @return -- exemplaire d'un livre
	 */
	public void endBorrow(Borrow borrow);
	

	public List<Borrow> getAllBorrow(Customer customer);
	
	public  void sendMail(boolean isCronTriggered, Booking booking);

}
