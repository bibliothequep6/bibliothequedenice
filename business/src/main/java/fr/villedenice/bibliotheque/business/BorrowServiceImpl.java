package fr.villedenice.bibliotheque.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import fr.villedenice.bibliotheque.consumer.*;
import fr.villedenice.bibliotheque.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BorrowServiceImpl implements BorrowService {
    @Autowired
    private WorkRepository workRepository;
    @Autowired
    private BorrowRepository borrowRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private BookingRepository bookingRepository;

    @Override
    public boolean startBorrow(Integer workId, Integer customerId) {
        boolean result = false;
        // Recuperer l'oeuvre � partir du workId --> workGot
        Work work = workRepository.findByWorkId(workId);
        Customer customer = customerRepository.findByCustomerId(customerId);
        // Recuperer l'ensemble des book de workGot --> bookList
        List<Book> bookList = work.getBooks();
        // Parcourir bookList --> for
        for (int i = 0; i < bookList.size(); i++) {
            Book book = bookList.get(i);
            if (book.isAvailable() == true) {

                Borrow borrow = new Borrow();
                borrow.setBorrowDate(new Date());
                Calendar limitDate = Calendar.getInstance();
                limitDate.add(Calendar.DATE, 28);
                borrow.setLimitDate(limitDate.getTime());
                borrow.setStatus(1);
                borrow.setCustomer(customer);
                borrow.setBook(book);
                borrowRepository.save(borrow);
                book.setAvailable(false);
                book.setBorrow(borrow);
                book.setWork(work);
                bookRepository.save(book);
                List<Borrow> customerBorrow = customer.getBorrow();
                customerBorrow.add(borrow);
                customer.setBorrow(customerBorrow);
                customerRepository.save(customer);
                result = true;

                break;
            }

        }
        // Pour chaque bookList --> book
        // if status book == 2
        // cr�er un emprunt (new Borrow())
        // lie le customer au borrow cr�e
        // lie le book au borrow cr�e
        // informations annexes --> date, status
        // sauvegarder en base borrow --> borrowtoreturn
        // break;

        // return borrowtoreturn

        return result;
    }

    public void extendBorrow(Borrow borrow) {
        // Récuperer l'emprunt à partir du borrowId
        // Borrow borrow = borrowRepository.findByBorrowId(borrowId);
        // Recuperer le statut de l'emprunt --> status
        int status = borrow.getStatus();
        Date today = new Date();

        // if status == 1
        if (status == 1 && borrow.getLimitDate().compareTo(today) > 0) {
            status = 3;
            borrow.setStatus(status);
            Calendar endDate = Calendar.getInstance();
            endDate.setTime(borrow.getLimitDate());
            endDate.add(Calendar.DATE, 28);
            borrow.setLimitDate(endDate.getTime());
            borrowRepository.save(borrow);
        }
        //todo service status
        // changer le status --> 3
        // prolonger la date de restitution --> +4 semaines (Calendar)
        // update en base borrow

    }

    @Override
    public void endBorrow(Borrow borrow) {

        Book book = bookRepository.findByBorrow(borrow);
        Calendar limitDate = Calendar.getInstance();
        borrow.setLimitDate(limitDate.getTime());
        borrow.setStatus(2);
        borrowRepository.save(borrow);
        book.setAvailable(true);
        book.setBorrow(null);
        bookRepository.save(book);
        List<Booking> bookingList = borrow.getBook().getWork().getBookingList();
        if (bookingList.size() > 0) {
            Booking booking = setBookingDate(bookingList.get(0));
            bookingRepository.save(booking);
            sendMail(false, booking);
        }
    }

    private Booking setBookingDate(Booking booking) {
        booking.setBookingStartDate(new Date());
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.DATE, 2);
        booking.setBookingEndDate(endDate.getTime());
        return booking;
    }

    @Override
    public Borrow getBorrowById(Integer borrowId) {
        Borrow borrow = borrowRepository.findByBorrowId(borrowId);
        return borrow;
    }

    @Override
    public List<Borrow> getAllBorrow(Customer customer) {

        List<Borrow> list = borrowRepository.findByCustomer(customer);
        return list;

    }

    @Override
    public void sendMail(boolean isCronTriggered, Booking booking) {

        /*List<Borrow> borrowList = new ArrayList<>();
        borrowRepository.findAll().forEach(e -> borrowList.add(e));*/
        List<Customer> customerList = new ArrayList<>();
        customerRepository.findAll().forEach(e -> customerList.add(e));
        Date today = new Date();
        String username = "bibliothequedenice@gmail.com";
        String password = "bdn123456";

        // 1 -> Création de la session
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true"); //todo set false test
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        if (isCronTriggered) {
            for (Customer customer : customerList) {
                List<Borrow> borrowList = customer.getBorrow();
                List<Borrow> toMessageList = new ArrayList<>();
                for (Borrow borrow : borrowList) {
                    Date limitDate = borrow.getLimitDate();
                    Calendar alertDate = Calendar.getInstance();
                    alertDate.setTime(limitDate);
                    alertDate.add(Calendar.DATE, -5);

                    if (today.compareTo(limitDate) >= 0 && borrow.getStatus() != 2) {
                        toMessageList.add(borrow);
                        continue;

                    } else if (today.compareTo(alertDate.getTime()) >= 0 && borrow.getStatus() != 2 && borrow.getCustomer().isAlert()) {
                        toMessageList.add(borrow);
                        continue;

                    }
                }
                if(toMessageList.size() != 0){
                    try {
                        MimeMessage message = createAlertMessage(toMessageList, session, customer);
                        // 3. Envoi du message
                        Transport.send(message);
                    } catch (MessagingException e1) {
                        e1.printStackTrace();
                    }
                }

            }

        } else {
            try {
                MimeMessage message = createBookingMessage(session, booking);
                // 3. Envoi du message
                Transport.send(message);
            } catch (MessagingException e1) {
                e1.printStackTrace();
            }
        }


    }

    private MimeMessage createAlertMessage(List<Borrow> borrowList, Session session, Customer customer) {
        Date today = new Date();
        try {
            MimeMessage message = new MimeMessage(session);
            String msgContent;
            msgContent = "Bonjour " + customer.getFirstName() + ", veuillez trouver ci dessous la" +
                    " liste de vos emprunts arrivant à expiration ou étant expiré : \n";
            for (Borrow borrow : borrowList) {
                Date limitDate = borrow.getLimitDate();
                Calendar alertDate = Calendar.getInstance();
                alertDate.setTime(limitDate);
                alertDate.add(Calendar.DATE, -5);

                if (today.compareTo(limitDate) >= 0 && borrow.getStatus() != 2) {
                    msgContent = msgContent.concat(borrow.getBook().getReference()+" est expiré \n");
                    continue;
                }
                else if (today.compareTo(alertDate.getTime()) >= 0 && borrow.getStatus() != 2 && borrow.getCustomer().isAlert()) {
                    msgContent = msgContent.concat(borrow.getBook().getReference()+" expire dans 5 jours \n");
                    continue;

                }


            }
            message.setText(msgContent);
            message.setSubject("Bibliothèque de Nice : Rappel");
            message.setFrom(new InternetAddress("bibliothequedenice@gmail.com"));
            InternetAddress[] address = {new InternetAddress(customer.getMail())};
            message.setRecipients(Message.RecipientType.TO, address);
            return message;
        } catch (MessagingException e1) {
            e1.printStackTrace();
            return null;
        }
    }

    private MimeMessage createEndBorrowMessage(List<Borrow> borrowList, Session session, Customer customer) {
        Date today = new Date();
        try {
            MimeMessage message = new MimeMessage(session);
            String msgContent;
            msgContent = "Bonjour " + customer.getFirstName() + ", veuillez trouver ci dessous la" +
                    " liste de vos emprunts ayant expiré : ";
            for (Borrow borrow : borrowList) {
                Date limitDate = borrow.getLimitDate();
                Calendar alertDate = Calendar.getInstance();
                alertDate.setTime(limitDate);
                alertDate.add(Calendar.DATE, -5);




            }

            message.setSubject("Bibliothèque de Nice : Expiration");
            message.setFrom(new InternetAddress("bibliothequedenice@gmail.com"));
            InternetAddress[] address = {new InternetAddress(customer.getMail())};
            message.setRecipients(Message.RecipientType.TO, address);
            return message;
        } catch (MessagingException e1) {
            e1.printStackTrace();
            return null;
        }
    }

    private MimeMessage createBookingMessage(Session session, Booking booking) {
        try {
            MimeMessage message = new MimeMessage(session);
            message.setText("Bonjour " + booking.getCustomer().getFirstName() + ", le livre : "
                    + booking.getWork().getTitle()
                    + " est de retour à la bibliothèque. " +
                    "Votre réservation est active pendant 48 heures à partir de l'heure de réception de ce mail.");

            message.setSubject("Bibliothèque de Nice : Votre réservation disponible");
            message.setFrom(new InternetAddress("bibliothequedenice@gmail.com"));
            InternetAddress[] address = {new InternetAddress(booking.getCustomer().getMail())};
            message.setRecipients(Message.RecipientType.TO, address);
            return message;
        } catch (MessagingException e1) {
            e1.printStackTrace();
            return null;
        }
    }

}
