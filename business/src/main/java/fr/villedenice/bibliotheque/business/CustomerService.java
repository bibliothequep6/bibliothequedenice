package fr.villedenice.bibliotheque.business;

import java.util.List;

import fr.villedenice.bibliotheque.model.*;

public interface CustomerService {
	/**
	 * Liste l'historique des emprunts de l'adh�rent
	 * 
	 * @param customer -- adh�rent
	 * @return -- liste d'emprunts
	 */
	public List<Borrow> getCustomerBorrowHistory(Integer customerId);
	
	public Customer findByMail(String mail);
	
	public Customer findByCustomerId(Integer customerId);

	public boolean logIn(String mail, String password);

	public List<Booking> getBookingList(Integer customerId);

	public Customer setAlert(Integer customerId);

	public void logOut();

}
