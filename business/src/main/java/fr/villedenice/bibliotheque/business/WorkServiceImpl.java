package fr.villedenice.bibliotheque.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import fr.villedenice.bibliotheque.consumer.BookingRepository;
import fr.villedenice.bibliotheque.consumer.CustomerRepository;
import fr.villedenice.bibliotheque.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.villedenice.bibliotheque.consumer.BookRepository;
import fr.villedenice.bibliotheque.consumer.WorkRepository;

@Service
public class WorkServiceImpl implements WorkService {
    @Autowired
    private WorkRepository workRepository;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private BorrowService borrowService;

    @Override
    public List<Work> findByAuthor(String authorName) {
        List<Work> obj = workRepository.findByAuthorContainsIgnoreCase(authorName);
        return obj;

    }

    @Override
    public List<Work> findByTitle(String workTitle) {
        List<Work> obj = workRepository.findByTitleContainsIgnoreCase(workTitle);
        return obj;
    }

    @Override
    public List<Work> findByYearPublication(int yearPublication) {
        List<Work> obj = workRepository.findByYearPublicationGreaterThanEqual(yearPublication);
        return obj;
    }

    @Override
    public List<Work> getAllWork() {
        List<Work> list = new ArrayList<>();
        workRepository.findAll().forEach(e -> list.add(e));
        return list;
    }

    @Override
    public String getNextReturnDate(Work work) {

        List<Book> bookList = work.getBooks();
        Date nextReturnDate = null;
        String nextReturnDateString = null;
        for (Book book : bookList) {
            if (book.isAvailable() == false) {
                if (nextReturnDate == null) {
                    Date limitDate = book.getBorrow().getLimitDate();
                    nextReturnDate = limitDate;
                } else {
                    Date limitDate = book.getBorrow().getLimitDate();
                    if (nextReturnDate.after(limitDate)) {
                        nextReturnDate = limitDate;
                    }
                }
            }
        }
        if (nextReturnDate != null) {
            nextReturnDateString = nextReturnDate.toString();
        }

        return nextReturnDateString;
    }

    @Override
    public boolean bookingWork(Integer workId, Integer customerId) {
        Work work = findByWorkId(workId);
        Customer customer = customerRepository.findByCustomerId(customerId);
        List<Booking> bookingList = work.getBookingList();
        List<Borrow> customerBorrowList = customer.getBorrow();
        boolean result = false;
        boolean flag = validateBooking(customerBorrowList, bookingList, customer, work);
        if (flag) {
            result = createBooking(customer, work);
        } else {
            return false;
        }

        return result;
    }
    //todo booking service
    @Override
    public void cancelBooking(Integer bookingId, Integer workId) {
        Booking booking = bookingRepository.findById(bookingId).get();
        bookingRepository.delete(booking);
        Work work = workRepository.findByWorkId(workId);
        List <Booking> bookingList = work.getBookingList();

        if(bookingList.size()>0 && isBookAvailable(work.getBooks()) &&bookingList.get(0)!=booking){
            borrowService.sendMail(false, bookingList.get(0));
        }

    }

    public boolean isBookAvailable(List<Book> bookList) {
        boolean result = false;
        for (Book book : bookList) {
            if (book.isAvailable()) {
                result = true;
            } else result = false;
        }
        return result;
    }

    public boolean createBooking(Customer customer, Work work) {
        Booking booking = new Booking();
        booking.setCustomer(customer);
        booking.setWork(work);
        bookingRepository.save(booking);
        customer.getBookingList().add(booking);
        customerRepository.save(customer);
        work.getBookingList().add(booking);
        workRepository.save(work);
        return true;
    }

    public boolean validateBooking(List<Borrow> customerBorrowList, List<Booking> bookingList, Customer customer, Work work) {
        for (Borrow borrow : customerBorrowList) {
            if (work.getTitle().equals(borrow.getBook().getReference())) {
                return false;
            }

        }
        for (Booking booking : bookingList) {
            if (booking.getCustomer().equals(customer)) {
                return false;
            }
        }
        if (bookingList.size() > work.getStockQuantity() * 2) {
            return false;
        }
        return true;
    }

    @Override
    public boolean addWork(Work work) {

        List<Work> list = workRepository.findByTitleContainsIgnoreCase(work.getTitle());
        if (list.size() > 0) {
            return false;
        } else {
            List<Book> books = new ArrayList<>();
            for (int i = work.getStockQuantity(); i > 0; i--) {
                Book book = new Book();
                book.setAvailable(true);
                book.setReference(work.getTitle());
                book.setWork(work);
                bookRepository.save(book);
                books.add(book);
            }
            work.setBooks(books);
            workRepository.save(work);
            return true;
        }
    }

    @Override
    public void updateWork(Work work) {
        workRepository.save(work);
    }

    @Override
    public Work findByWorkId(int workId) {
        Work work = workRepository.findByWorkId(workId);
        return work;
    }

    @Override
    public List<Work> findByFilter(String author, String title, Integer year) {
        //todo refacto?
        List<Work> workList = new ArrayList<>();
        //Cas : Titre et année
        if (author.isEmpty() && !title.isEmpty() && !year.toString().isEmpty()) {
            workList = workRepository.findByTitleIgnoreCaseContainsAndYearPublicationGreaterThanEqual(title, year);
        }
        //Cas : Auteur et année
        else if (title.isEmpty() && !author.isEmpty() && !year.toString().isEmpty()) {
            workList = workRepository.findByAuthorIgnoreCaseContainsAndYearPublicationGreaterThanEqual(author, year);
        }
        //Cas : Auteur et titre
        else if (year.toString().isEmpty() && !title.isEmpty() && !author.isEmpty()) {
            workList = workRepository.findByAuthorIgnoreCaseContainsAndTitleIgnoreCaseContains(author, title);
        }
        //Cas : Auteur uniquement
        else if (!author.isEmpty() && title.isEmpty() && year.toString().isEmpty()) {
            workList = workRepository.findByAuthorContainsIgnoreCase(author);
        }
        //Cas : Titre uniquement
        else if (author.isEmpty() && !title.isEmpty() && year.toString().isEmpty()) {
            workList = workRepository.findByTitleContainsIgnoreCase(title);
        }
        //Cas : Année uniquement
        else if (author.isEmpty() && title.isEmpty() && !year.toString().isEmpty()) {
            workList = workRepository.findByYearPublicationGreaterThanEqual(year);
        } else if (!author.isEmpty() && !title.isEmpty() && !year.toString().isEmpty()) {
            workList = workRepository.findByAuthorIgnoreCaseContainsAndTitleIgnoreCaseContainsAndYearPublicationGreaterThanEqual(author, title, year);
        } else {
            workList = getAllWork();
        }
        return workList;
    }


}
