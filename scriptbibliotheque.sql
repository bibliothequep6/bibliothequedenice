-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 04 mars 2019 à 14:37
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de données :  `bibliotheque`
--
CREATE DATABASE IF NOT EXISTS `bibliotheque` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bibliotheque`;

-- --------------------------------------------------------

--
-- Structure de la table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `borrow_borrow_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`book_id`),
  KEY `FK6xg8c1w6funsk3xnds6c8m7ds` (`borrow_borrow_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `book`
--

INSERT INTO `book` (`book_id`, `available`, `reference`, `borrow_borrow_id`) VALUES
(1, b'0', 'Charlie et la chocolaterie', 13),
(2, b'0', 'Harry Potter à l\'école des sorciers', 14),
(3, b'0', 'Harry Potter à l\'école des sorciers', 12),
(26, b'0', 'Harry Potter et la chambre des secrets', 15),
(27, b'0', 'Harry Potter et la chambre des secrets', 6),
(28, b'1', 'Deviens un Ninja avec Angular', NULL),
(29, b'1', 'Deviens un Ninja avec Angular', NULL),
(30, b'1', 'Deviens un Ninja avec Angular', NULL),
(31, b'0', 'Divergente 1 : Divergent', 9),
(32, b'1', 'Divergente 1 : Divergent', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `booktype`
--

CREATE TABLE IF NOT EXISTS `booktype` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `booktype`
--

INSERT INTO `booktype` (`type_id`, `title`) VALUES
(1, 'Comedie'),
(2, 'Romantique'),
(3, 'Fantastique'),
(4, 'Science-Fiction');

-- --------------------------------------------------------

--
-- Structure de la table `booktype_work`
--

CREATE TABLE IF NOT EXISTS `booktype_work` (
  `booktype_type_id` int(11) NOT NULL,
  `work_work_id` int(11) NOT NULL,
  UNIQUE KEY `UK_q5iggd73niuhixui8tcq0e5qq` (`work_work_id`),
  KEY `FKe3f6wen7h4fksomnnjlo1tq8v` (`booktype_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `booktype_work`
--

INSERT INTO `booktype_work` (`booktype_type_id`, `work_work_id`) VALUES
(1, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `borrow`
--

CREATE TABLE IF NOT EXISTS `borrow` (
  `borrow_id` int(11) NOT NULL AUTO_INCREMENT,
  `borrow_date` date DEFAULT NULL,
  `limit_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `book_book_id` int(11) DEFAULT NULL,
  `customer_customer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`borrow_id`),
  KEY `FKsechakti9779s2jl4v1k5psxs` (`book_book_id`),
  KEY `FKjklguc9ttco4e3xkf12p3wep3` (`customer_customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `borrow`
--

INSERT INTO `borrow` (`borrow_id`, `borrow_date`, `limit_date`, `status`, `book_book_id`, `customer_customer_id`) VALUES
(2, '2019-01-01', '2019-01-17', 2, 1, 1),
(3, '2019-01-17', '2019-02-15', 2, 2, 2),
(4, '2019-01-17', '2019-01-17', 2, 3, 2),
(5, '2019-01-17', '2019-02-16', 2, 26, 2),
(6, '2019-01-17', '2019-02-14', 1, 27, 1),
(7, '2019-01-21', '2019-03-01', 2, 28, 1),
(8, '2019-01-21', '2019-01-21', 2, 29, 1),
(9, '2019-01-25', '2019-02-22', 1, 31, 1),
(10, '2019-02-08', '2019-02-21', 2, 29, 2),
(11, '2019-02-11', '2019-02-15', 2, 32, 2),
(12, '2019-02-11', '2019-03-11', 1, 3, 2),
(13, '2019-02-15', '2019-04-11', 3, 1, 2),
(14, '2019-02-21', '2019-03-21', 1, 2, 2),
(15, '2019-01-29', '2019-02-26', 1, 26, 2);

-- --------------------------------------------------------

--
-- Structure de la table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `customer`
--

INSERT INTO `customer` (`customer_id`, `address`, `first_name`, `last_name`, `mail`, `password`) VALUES
(1, '12 chemin du lavoir', 'Gerard', 'Depardeu', 'gerarddepardeu@gmail.com', 'azertyuiop'),
(2, '45 chemin de l\'eglise', 'Monique', 'Mimosas', 'mimosasmonique@gmail.com', 'azertyuiop');

-- --------------------------------------------------------

--
-- Structure de la table `customer_borrow`
--

CREATE TABLE IF NOT EXISTS `customer_borrow` (
  `customer_customer_id` int(11) NOT NULL,
  `borrow_borrow_id` int(11) NOT NULL,
  UNIQUE KEY `UK_nph5hnmm63bvfigs1wfsvs7es` (`borrow_borrow_id`),
  KEY `FKpc56wjr3ek300lfabvnhh4t78` (`customer_customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `customer_borrow`
--

INSERT INTO `customer_borrow` (`customer_customer_id`, `borrow_borrow_id`) VALUES
(1, 2),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(2, 3),
(2, 4),
(2, 5),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15);

-- --------------------------------------------------------

--
-- Structure de la table `librarian`
--

CREATE TABLE IF NOT EXISTS `librarian` (
  `librarianid` int(11) NOT NULL,
  PRIMARY KEY (`librarianid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `librarian`
--

INSERT INTO `librarian` (`librarianid`) VALUES
(2);

-- --------------------------------------------------------

--
-- Structure de la table `library`
--

CREATE TABLE IF NOT EXISTS `library` (
  `library_id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`library_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `library`
--

INSERT INTO `library` (`library_id`, `city`, `name`) VALUES
(1, 'Nice', 'Bibliothèque Arnaud Choplin');

-- --------------------------------------------------------

--
-- Structure de la table `library_librarian`
--

CREATE TABLE IF NOT EXISTS `library_librarian` (
  `library_library_id` int(11) NOT NULL,
  `librarian_librarianid` int(11) NOT NULL,
  UNIQUE KEY `UK_3iciqtodib2kwa7oj4se4gn8l` (`librarian_librarianid`),
  KEY `FK4ylv2cpv3qi3ac3wj5q29qiwk` (`library_library_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `library_librarian`
--

INSERT INTO `library_librarian` (`library_library_id`, `librarian_librarianid`) VALUES
(1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `library_work`
--

CREATE TABLE IF NOT EXISTS `library_work` (
  `library_library_id` int(11) NOT NULL,
  `work_work_id` int(11) NOT NULL,
  UNIQUE KEY `UK_214y9bxlqgdlhgc5fep6wq9v5` (`work_work_id`),
  KEY `FKmeviedx36scxyr5ulwjtwfgfu` (`library_library_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `library_work`
--

INSERT INTO `library_work` (`library_library_id`, `work_work_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `work`
--

CREATE TABLE IF NOT EXISTS `work` (
  `work_id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) DEFAULT NULL,
  `stock_quantity` int(11) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `year_publication` int(11) DEFAULT NULL,
  `type_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`work_id`),
  KEY `FK9aifgpr4nk6ku2tvkn2yhf629` (`type_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `work`
--

INSERT INTO `work` (`work_id`, `author`, `stock_quantity`, `summary`, `title`, `year_publication`, `type_type_id`) VALUES
(1, 'Roald Dahl', 1, 'Charlie a gagné le ticket d\'or pour visiter la chocolaterie', 'Charlie et la chocolaterie', 1964, 1),
(2, 'JK Rowling', 2, 'Harry Potter découvre qu\'il est un sorcier', 'Harry Potter à l\'école des sorciers', 2001, 3),
(15, 'JK Rowling', 2, 'Harry Potter attaque sa deuxième année à Poudlard.', 'Harry Potter et la chambre des secrets', 2002, NULL),
(16, 'Ninja Squad', 3, 'Le nouveau cours de Ninja Squad sur la dernière version du framework de Google', 'Deviens un Ninja avec Angular', 2018, NULL),
(17, 'Veronica Roth', 2, 'Dans une ville détruite enfermée dans une cloture les hommes se divisent en quatres factions. Afin que règne la paix et l\'ordre, chaque faction doit remplir son role.', 'Divergente 1 : Divergent', 2008, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `work_book`
--

CREATE TABLE IF NOT EXISTS `work_book` (
  `work_work_id` int(11) NOT NULL,
  `book_book_id` int(11) NOT NULL,
  UNIQUE KEY `UK_etuwkv340790hpk3yhaj21pfd` (`book_book_id`),
  KEY `FKefvcgoyrcdukb1flj90n3ifxq` (`work_work_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `work_book`
--

INSERT INTO `work_book` (`work_work_id`, `book_book_id`) VALUES
(1, 1),
(2, 2),
(2, 3),
(15, 26),
(15, 27),
(16, 28),
(16, 29),
(16, 30),
(17, 31),
(17, 32);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `FK6xg8c1w6funsk3xnds6c8m7ds` FOREIGN KEY (`borrow_borrow_id`) REFERENCES `borrow` (`borrow_id`);

--
-- Contraintes pour la table `booktype_work`
--
ALTER TABLE `booktype_work`
  ADD CONSTRAINT `FK20oc6rvc6ecexeud1eeqlrngi` FOREIGN KEY (`work_work_id`) REFERENCES `work` (`work_id`),
  ADD CONSTRAINT `FKe3f6wen7h4fksomnnjlo1tq8v` FOREIGN KEY (`booktype_type_id`) REFERENCES `booktype` (`type_id`);

--
-- Contraintes pour la table `borrow`
--
ALTER TABLE `borrow`
  ADD CONSTRAINT `FKjklguc9ttco4e3xkf12p3wep3` FOREIGN KEY (`customer_customer_id`) REFERENCES `customer` (`customer_id`),
  ADD CONSTRAINT `FKsechakti9779s2jl4v1k5psxs` FOREIGN KEY (`book_book_id`) REFERENCES `book` (`book_id`);

--
-- Contraintes pour la table `customer_borrow`
--
ALTER TABLE `customer_borrow`
  ADD CONSTRAINT `FK23pxiqhcp8f8rjvaq74jb0yf4` FOREIGN KEY (`borrow_borrow_id`) REFERENCES `borrow` (`borrow_id`),
  ADD CONSTRAINT `FKpc56wjr3ek300lfabvnhh4t78` FOREIGN KEY (`customer_customer_id`) REFERENCES `customer` (`customer_id`);

--
-- Contraintes pour la table `librarian`
--
ALTER TABLE `librarian`
  ADD CONSTRAINT `FKqfxe91p29rihi731w5yxtyte7` FOREIGN KEY (`librarianid`) REFERENCES `customer` (`customer_id`);
COMMIT;
