package fr.villedenice.bibliotheque.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity(name = "work")
@Table(name = "work")
public class Work implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "work_id")
    private Integer workId;
    @Column(name = "title")
    private String title;
    @Column(name = "author")
    private String author;
    @Column(name = "summary")
    private String summary;
    @Column(name = "stock_quantity")
    private int stockQuantity;
    @Column(name = "year_publication")
    private int yearPublication;
    @ManyToOne
    private BookType type;
    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Book> book;
    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Booking> bookingList;


    public Work(int stockQuantity, int yearPublication, String title, String author, String summary, Integer id) {
        super();

        this.stockQuantity = stockQuantity;
        this.yearPublication = yearPublication;
        this.title = title;
        this.author = author;
        this.summary = summary;
        this.workId = id;
    }

    public Work() {

    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public int getYearPublication() {
        return yearPublication;
    }

    public void setYearPublication(int yearPublication) {
        this.yearPublication = yearPublication;
    }

    public Integer getWorkId() {
        return workId;
    }

    public void setWorkId(Integer id) {
        this.workId = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public BookType getType() {
        return type;
    }

    public void setType(BookType type) {
        this.type = type;
    }

    public List<Book> getBooks() {
        return book;
    }

    public void setBooks(List<Book> books) {
        this.book = books;
    }

    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }
}
