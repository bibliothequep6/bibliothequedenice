package fr.villedenice.bibliotheque.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity(name="library")
@Table(name = "library")
public class Library implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer libraryId;
	private String name;
	private String city;
	@OneToMany
	private List<Work> work;
	@OneToMany
	private List<Librarian> librarian;
	
	

	public Library(Integer id, String name, String city) {
		super();
		this.libraryId = id;
		this.name = name;
		this.city = city;

	}

	public Library() {

	}

	public Integer getId() {
		return libraryId;
	}

	public void setId(Integer id) {
		this.libraryId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public List<Work> getWork() {
		return work;
	}

	public void setWorks(List<Work> work) {
		this.work = work;
	}


}
