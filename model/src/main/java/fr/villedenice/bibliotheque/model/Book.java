package fr.villedenice.bibliotheque.model;

import java.io.Serializable;

import javax.persistence.*;




@Entity(name="book")
@Table(name = "book")
public class Book implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer bookId;
	@Column(name="reference")
	private String reference;
	@Column(name="available")
	private boolean available;
	@OneToOne
	private Borrow borrow;
	@ManyToOne
	private Work work;
	
	public Book(Integer id, String reference, boolean available, BookType type) {
		super();
		this.bookId = id;
		this.reference = reference;
		this.available = available;
	}

	public Book() {

	}

	public Integer getBookId() {
		return bookId;
	}

	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Borrow getBorrow() {
		return borrow;
	}

	public void setBorrow(Borrow borrow) {
		this.borrow = borrow;
	}

	public Work getWork() {
		return work;
	}

	public void setWork(Work work) {
		this.work = work;
	}
}
