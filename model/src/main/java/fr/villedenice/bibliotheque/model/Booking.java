package fr.villedenice.bibliotheque.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "booking")
@Table(name = "booking")
public class Booking implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "booking_id")
    private Integer id;
    @Column(name="booking_start_date")
    private Date bookingStartDate;
    @Column(name="booking_end_date")
    private Date bookingEndDate;
    @ManyToOne
    private Customer customer;
    @ManyToOne
    private Work work;


    public Booking() {
    }

    public Booking(Date bookingStartDate, Date bookingEndDate, Customer customer) {
        this.bookingStartDate = bookingStartDate;
        this.bookingEndDate = bookingEndDate;
        this.customer = customer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getBookingStartDate() {
        return bookingStartDate;
    }

    public void setBookingStartDate(Date bookingStartDate) {
        this.bookingStartDate = bookingStartDate;
    }

    public Date getBookingEndDate() {
        return bookingEndDate;
    }

    public void setBookingEndDate(Date bookingEndDate) {
        this.bookingEndDate = bookingEndDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Work getWork() {
        return work;
    }

    public void setWork(Work work) {
        this.work = work;
    }
}
