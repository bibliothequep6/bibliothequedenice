package fr.villedenice.bibliotheque.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity(name = "booktype")
@Table(name = "booktype")
public class BookType implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer typeId;
	private String title;
	@OneToMany(cascade = CascadeType.ALL)
	private List<Work> work;

	

	public BookType(Integer id, String title) {
		super();
		this.typeId = id;
		this.title = title;
	}

	public BookType() {

	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Work> getWork() {
		return work;
	}

	public void setWork(List<Work> work) {
		this.work = work;
	}

}
