package fr.villedenice.bibliotheque.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity(name = "librarian")
@Table(name = "librarian")
@PrimaryKeyJoinColumn(name = "librarianid")
public class Librarian extends Customer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Librarian() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Librarian(String first_name, String last_name, String address, String mail, String password) {
		super(first_name, last_name, address, mail, password);
		// TODO Auto-generated constructor stub
	}
	


}
