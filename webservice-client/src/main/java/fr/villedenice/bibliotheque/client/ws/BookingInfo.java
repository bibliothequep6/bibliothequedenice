
package fr.villedenice.bibliotheque.client.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour bookingInfo complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="bookingInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bookingId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="bookingStartDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="bookingEndDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="customer" type="{http://www.villedenicebibliotheque.fr/work-ws}customerInfo" minOccurs="0"/>
 *         &lt;element name="work" type="{http://www.villedenicebibliotheque.fr/work-ws}workInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bookingInfo", propOrder = {
    "bookingId",
    "bookingStartDate",
    "bookingEndDate",
    "customer",
    "work"
})
public class BookingInfo {

    protected int bookingId;
    @XmlElement(required = true)
    protected String bookingStartDate;
    @XmlElement(required = true)
    protected String bookingEndDate;
    protected CustomerInfo customer;
    protected WorkInfo work;

    /**
     * Obtient la valeur de la propriété bookingId.
     * 
     */
    public int getBookingId() {
        return bookingId;
    }

    /**
     * Définit la valeur de la propriété bookingId.
     * 
     */
    public void setBookingId(int value) {
        this.bookingId = value;
    }

    /**
     * Obtient la valeur de la propriété bookingStartDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingStartDate() {
        return bookingStartDate;
    }

    /**
     * Définit la valeur de la propriété bookingStartDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingStartDate(String value) {
        this.bookingStartDate = value;
    }

    /**
     * Obtient la valeur de la propriété bookingEndDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingEndDate() {
        return bookingEndDate;
    }

    /**
     * Définit la valeur de la propriété bookingEndDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingEndDate(String value) {
        this.bookingEndDate = value;
    }

    /**
     * Obtient la valeur de la propriété customer.
     * 
     * @return
     *     possible object is
     *     {@link CustomerInfo }
     *     
     */
    public CustomerInfo getCustomer() {
        return customer;
    }

    /**
     * Définit la valeur de la propriété customer.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerInfo }
     *     
     */
    public void setCustomer(CustomerInfo value) {
        this.customer = value;
    }

    /**
     * Obtient la valeur de la propriété work.
     * 
     * @return
     *     possible object is
     *     {@link WorkInfo }
     *     
     */
    public WorkInfo getWork() {
        return work;
    }

    /**
     * Définit la valeur de la propriété work.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkInfo }
     *     
     */
    public void setWork(WorkInfo value) {
        this.work = value;
    }

}
