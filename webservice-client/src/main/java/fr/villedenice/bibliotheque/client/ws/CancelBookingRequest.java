
package fr.villedenice.bibliotheque.client.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="workId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="bookingId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "workId",
    "bookingId"
})
@XmlRootElement(name = "cancelBookingRequest")
public class CancelBookingRequest {

    protected int workId;
    protected int bookingId;

    /**
     * Obtient la valeur de la propriété workId.
     * 
     */
    public int getWorkId() {
        return workId;
    }

    /**
     * Définit la valeur de la propriété workId.
     * 
     */
    public void setWorkId(int value) {
        this.workId = value;
    }

    /**
     * Obtient la valeur de la propriété bookingId.
     * 
     */
    public int getBookingId() {
        return bookingId;
    }

    /**
     * Définit la valeur de la propriété bookingId.
     * 
     */
    public void setBookingId(int value) {
        this.bookingId = value;
    }

}
