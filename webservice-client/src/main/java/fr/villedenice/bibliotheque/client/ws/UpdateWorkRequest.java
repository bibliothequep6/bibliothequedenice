
package fr.villedenice.bibliotheque.client.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="workInfo" type="{http://www.villedenicebibliotheque.fr/work-ws}workInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "workInfo"
})
@XmlRootElement(name = "updateWorkRequest")
public class UpdateWorkRequest {

    @XmlElement(required = true)
    protected WorkInfo workInfo;

    /**
     * Obtient la valeur de la propriété workInfo.
     * 
     * @return
     *     possible object is
     *     {@link WorkInfo }
     *     
     */
    public WorkInfo getWorkInfo() {
        return workInfo;
    }

    /**
     * Définit la valeur de la propriété workInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkInfo }
     *     
     */
    public void setWorkInfo(WorkInfo value) {
        this.workInfo = value;
    }

}
