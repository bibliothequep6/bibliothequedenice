package fr.villedenice.batch;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import fr.villedenice.bibliotheque.client.ws.SendMailRequest;
import fr.villedenice.bibliotheque.client.ws.WorkPort;
import fr.villedenice.bibliotheque.client.ws.WorkPortService;

public class SendEmailJob implements Job {
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// calling client from webservice
		WorkPortService workPortService = new WorkPortService();
		WorkPort workPort = workPortService.getWorkPortSoap11();

		// calling method from webservice
		SendMailRequest sendMailRequest = new SendMailRequest();
		workPort.sendMail(sendMailRequest);
		System.out.println("-------executing job---------");
	}


}
