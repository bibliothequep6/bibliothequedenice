package fr.villedenice.batch;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class Main {

	public static void main(String[] args) {

		// the definition of the job instance..by pointing to the sendemailjob class
		// (class implementing Job)

		JobDetail sendjob = JobBuilder.newJob(SendEmailJob.class).withIdentity("sendjob", "group1").build();

		// using cron expression to specify the times to run the job.here is every min:00

		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("triggersendjob", "group1")
				.withSchedule(CronScheduleBuilder.cronSchedule("0 0/1 * * * ?")).build();
// schedule the job
		try {
			Scheduler scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJob(sendjob, trigger);


		} catch (SchedulerException e) {
			e.printStackTrace();

		}

	}
}
