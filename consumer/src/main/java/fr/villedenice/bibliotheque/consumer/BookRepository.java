package fr.villedenice.bibliotheque.consumer;

import org.springframework.data.repository.CrudRepository;

import fr.villedenice.bibliotheque.model.Book;
import fr.villedenice.bibliotheque.model.Borrow;

public interface BookRepository extends CrudRepository<Book, Integer> {
	Book findByBookId(Integer bookId);

	Book findByBorrow(Borrow borrow);

}
