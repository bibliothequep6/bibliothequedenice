package fr.villedenice.bibliotheque.consumer;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.villedenice.bibliotheque.model.Work;


public interface WorkRepository extends CrudRepository<Work, Integer> {
	List<Work> findByAuthorContainsIgnoreCase(String author);

	List<Work> findByTitleContainsIgnoreCase(String title);

	List<Work> findByYearPublicationGreaterThanEqual(Integer yearPublication);
	
	Work findByWorkId (Integer workId);
	//Cas full champs
	List<Work> findByAuthorIgnoreCaseContainsAndTitleIgnoreCaseContainsAndYearPublicationGreaterThanEqual(String author, String title, Integer yearPublication);
	//Cas auteur et titre
	List<Work> findByAuthorIgnoreCaseContainsAndTitleIgnoreCaseContains(String author, String title);
	//Cas auteur et année
	List<Work> findByAuthorIgnoreCaseContainsAndYearPublicationGreaterThanEqual(String author, Integer yearPublication);
	//Cas titre et année
	List<Work> findByTitleIgnoreCaseContainsAndYearPublicationGreaterThanEqual(String title, Integer yearPublication);

}
