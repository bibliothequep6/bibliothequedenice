package fr.villedenice.bibliotheque.consumer;

import fr.villedenice.bibliotheque.model.Booking;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookingRepository extends CrudRepository<Booking, Integer> {
    List<Booking> findByCustomerCustomerId(Integer customerId);
}
