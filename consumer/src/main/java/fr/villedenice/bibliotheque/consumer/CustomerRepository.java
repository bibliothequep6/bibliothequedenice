package fr.villedenice.bibliotheque.consumer;

import org.springframework.data.repository.CrudRepository;

import fr.villedenice.bibliotheque.model.Borrow;
import fr.villedenice.bibliotheque.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
	
	Customer findByMail(String mail);
	Customer findByCustomerId(Integer customerId);
	Customer findByBorrow(Borrow borrow);

}
