package fr.villedenice.bibliotheque.consumer;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.villedenice.bibliotheque.model.Borrow;
import fr.villedenice.bibliotheque.model.Customer;



public interface BorrowRepository extends CrudRepository<Borrow, Integer> {

	Borrow findByBorrowId(Integer borrowId);
	
	List<Borrow> findByCustomer(Customer customer);
 
}
