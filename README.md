Ce projet nécessite :
*                       -maven 3.6.0 installé dans votre environnement de développement (voir : https://maven.apache.org/install.html)
*                       -tomcat 8.5 installé dans votre environnement de développement (voir :https://tomcat.apache.org/tomcat-8.0-doc/setup.html)
*                       -java jdk1.8


1.  Telecharger le dossier

-------------------------------Creation de la base de données--------------------------------------

2.  Creer la base de données (mysql) à partir du script sql "scriptbibliotheque.sql" et mettre en information de connexion à la base user:root, password=pass. 
(ou modifier avec vos informations de connexion les valeurs spring.datasource.username=**root** spring.datasource.password=**pass** dans le fichier *webserviceproducer/src/main/resources/application.properties*)

-------------------------------Build du projet-----------------------------------------------------

3.  Ouvrir la console et aller à la source du dossier
4.  Entrer la ligne de commande : mvn install

-------------------------------Lancement du webservice---------------------------------------------

5.  Toujours dans la console aller dans le dossier "webserviceproducer"
6.  Entrer la ligne de commande : mvn spring-boot:run
7.  Ouvrir une deuxieme console aller dans le dossier "webserviceclient" et ecrire la ligne de commande : mvn install

-------------------------------Deploiement de l'application web------------------------------------

8.  Copier le dossier webapp et le webapp.war depuis webapp/target vers le sous-dossier webapps de votre installation tomcat
9.  Ouvrir une troisieme console, aller au dossier d'installation tomcat/bin et ecrire en ligne de commande catalina.bat start
10.  Le site est trouvable a l'adresse : localhost:8181/webapp/ 
11.  Possibilité de tester les fonctions avec les utilisateurs : gerarddepardeu@gmail.com, et mimosasmonique@gmail.com, les mots de passes etant azertyuiop

-------------------------------Lancement du batch---------------------------------------------------

12.  Ouvrir une quatrieme console, aller dans le dossier bibliotheque/batch et lancer la ligne de commande : java -jar batch-0.0.1-SNAPSHOT-jar-with-dependencies (il est possible que vous ayez a désactiver votre antivirus pour que cela fonctionne)
13.  Un seul mail s'enverra sur l'adresse gmail mimosasmonique@gmail.com mdp: bdn123456 toute les minutes (adresse crée pour tester le batch sur ce membre uniquement).